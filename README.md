# Docker Development Environment BETA [![Version ](https://img.shields.io/badge/version-2.0.0_BETA_-brightgreen.svg?style=flat)](https://bitbucket.org/HTML24DK/docker-development-environment/src/master/)

## How to setup a site with this.

1. Download all files releated to the site and copy them inside web

2. Download the database dump and put the SQL file inside /put-database-dump-here/

3. Open the terminal and navigate to where you cloned this repo and run 
```sh
docker-compose up
```

A project should look like this

![alt text](https://bitbucket.org/HTML24DK/docker-development-environment/raw/59c3cd7bf3bb01ff5fcd46d79461eda13998c6de/working-example.png)


## How to use GIT with this setup (OPTIONAL)

1. Clone a theme into /web/wp-content/themes/



## Configure gulp and composer (OPTIONAL)


1. Open .env

2. Set THEME_LOCATION - To where your themes are located (usally this never changes and is most likely already set correctly)

3. Set THEME_NAME - The folder of which contain the theme /web/wp-content/themes/{THEME_NAME}
   




Docker running Nginx, PHP-FPM, Composer, MySQL and PHPMyAdmin.

## Overview

1. [Install prerequisites](#install-prerequisites)

    Before installing project make sure the following prerequisites have been met.

2. [Clone the project](#clone-the-project)

    We’ll download the code from its repository on GitHub.

3. [Configure Nginx With SSL Certificates](#configure-nginx-with-ssl-certificates) [Optional]

    We'll generate and configure SSL certificate for nginx before running server.

4. [Configure Xdebug](#configure-xdebug) [Optional]

    We'll configure Xdebug for IDE (PHPStorm or Netbeans).

5. [Run the application](#run-the-application)

    By this point we’ll have all the project pieces in place.

6. [Use Docker Commands](#use-docker-commands)

    When running, you can use docker commands for doing recurrent operations.

7. [Issues and fixes](#issues-and-fixes)

    If you run into any "common" issues they might already be documented here.

___

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Git](https://git-scm.com/downloads) (optional)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command : 

```sh
which docker-compose
```

Check Docker Compose compatibility :

 - [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

On Ubuntu and Debian these are available in the meta-package build-essential. On other distributions, you may need to install the GNU C++ compiler separately.

```sh
sudo apt install build-essential
```

### Images to use

* [Nginx](https://hub.docker.com/_/nginx/)
* [MySQL](https://hub.docker.com/_/mysql/)
* [PHP-FPM](https://hub.docker.com/r/nanoninja/php-fpm/)
* [Composer](https://hub.docker.com/_/composer/)
* [PHPMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)
* [Generate Certificate](https://hub.docker.com/r/jacoelho/generate-certificate/)

You should be careful when installing third party web servers such as MySQL or Nginx.

This project use the following ports :

| Server     | Port |
|------------|------|
| MySQL      | 8989 |
| PHPMyAdmin | 8080 |
| Nginx      | 80   |
| Nginx SSL  | 443  |

---

## Clone the project

To install [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git), download it and install following the instructions : 

```sh
git clone https://github.com/example/
```

Go to the project directory : 

```sh
cd example
```

### Project tree

```sh
.
├── README.md
├── data
│   └── db
│       ├── dumps
│       └── mysql
├── doc
├── docker-compose.yml
├── etc
│   ├── nginx
│   │   ├── default.conf
│   │   ├── default.template.conf
│   │   ├── wp.conf
│   │   └── drupal.conf
│   ├── php
│   │   └── php.ini
│   └── ssl
└── web
```

---

## Configure Nginx With SSL Certificates

You can change the host name by editing the `.env` file.

If you modify the host name, do not forget to add it to the `/etc/hosts` file.

1. Generate SSL certificates

    ```sh
    source .env && sudo docker run --rm -v $(pwd)/etc/ssl:/certificates -e "SERVER=$NGINX_HOST" jacoelho/generate-certificate
    ```

2. Configure Nginx

    Do not modify the `etc/nginx/default.conf` file, it is overwritten by  `etc/nginx/default.template.conf`

    Edit nginx file `etc/nginx/default.template.conf` and uncomment the SSL server section :

    ```sh
    # server {
    #     server_name ${NGINX_HOST};
    #
    #     listen 443 ssl;
    #     fastcgi_param HTTPS on;
    #     ...
    # }
    ```

---

## Configure Xdebug

If you use another IDE than [PHPStorm](https://www.jetbrains.com/phpstorm/) or [Netbeans](https://netbeans.org/), go to the [remote debugging](https://xdebug.org/docs/remote) section of Xdebug documentation.

For a better integration of Docker to PHPStorm, use the [documentation](https://github.com/nanoninja/docker-nginx-php-mysql/blob/master/doc/phpstorm-macosx.md).

1. Get your own local IP address :

    ```sh
    sudo ifconfig
    ```

2. Edit php file `etc/php/php.ini` and comment or uncomment the configuration as needed.

3. Set the `remote_host` parameter with your IP :

    ```sh
    xdebug.remote_host=192.168.0.1 # your IP
    ```
---


## Run the application


1. Start the application :

    ```sh
    sudo docker-compose up -d
    ```

    **Please wait this might take a several minutes...**

    ```sh
    sudo docker-compose logs -f # Follow log output
    ```

2. Open your favorite browser :

    * [http://localhost:80](http://localhost:80/)
    * [https://localhost:443](https://localhost:443/) ([HTTPS](#configure-nginx-with-ssl-certificates) not configured by default)
    * [http://localhost:8080](http://localhost:8080/) PHPMyAdmin (username: dev, password: dev)

3. Stop and clear services

    ```sh
    sudo docker-compose down -v
    ```

---

## Use Docker commands

### Installing package with composer

```sh
sudo docker run --rm -v $(pwd)/web/app:/app composer require symfony/yaml
```

### Updating PHP dependencies with composer

```sh
sudo docker run --rm -v $(pwd)/web/app:/app composer update
```

### Generating PHP API documentation

```sh
sudo docker-compose exec -T php ./app/vendor/bin/apigen generate app/src --destination ./app/doc
```

### Testing PHP application with PHPUnit

```sh
sudo docker-compose exec -T php ./app/vendor/bin/phpunit --colors=always --configuration ./app/
```

### Checking the standard code with [PSR2](http://www.php-fig.org/psr/psr-2/)

```sh
sudo docker-compose exec -T php ./app/vendor/bin/phpcs -v --standard=PSR2 ./app/src
```

### Checking installed PHP extensions

```sh
sudo docker-compose exec php php -m
```

### Handling database

#### MySQL shell access

```sh
sudo docker exec -it mysql bash
```

and

```sh
mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD"
```

#### Backup of database

```sh
mkdir -p data/db/dumps
```

```sh
source .env && sudo docker exec $(sudo docker-compose ps -q mysqldb) mysqldump --all-databases -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "data/db/dumps/db.sql"
```

or

```sh
source .env && sudo docker exec $(sudo docker-compose ps -q mysqldb) mysqldump test -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "data/db/dumps/test.sql"
```

#### Restore Database

```sh
source .env && sudo docker exec -i $(sudo docker-compose ps -q mysqldb) mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" < "data/db/dumps/db.sql"
```

#### Connecting MySQL from [PDO](http://php.net/manual/en/book.pdo.php)

```php
<?php
    try {
        $dsn = 'mysql:host=mysql;dbname=test;charset=utf8;port=3306';
        $pdo = new PDO($dsn, 'dev', 'dev');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
?>
```
#### PHP 7 Alpine Snippet from [PDO](http://php.net/manual/en/book.pdo.php)

```php
    php:
        build: ./etc/phpfpm
        env_file:
            - ".env"
        restart: always
        user: 1000:1000
        command: sh /var/www/set-urls.sh
        volumes:
            - "./etc/php/set-urls.sh:/var/www/set-urls.sh"
            - "./etc/php/php.ini:/etc/php7/php.ini"
            - ./web:/var/www/html:delegated
            - ./etc/phpfpm/www.conf:/etc/php7/php-fpm.d/www.conf
            - ./put-database-dump-here/:/var/www/db/
```

---
## Notes

Wordpress


## Issues and fixes

### 403 on linux

This is very likely due the permissions of the folder which contain the site aren't set correctly.

To fix this open the terminal and navigate to the folder where this file is and write.

```chmod -R o+rX ./
```


## CHANGELOG
### 1.0 - Projected started 
### 1.1 
Removed -   Uses WordPress image
Added   -   NGINX image
Added   -   NGINX config files to WordPress
Fixed   -   css and font files wouldnt load
Added   -   xDebug support
Added   -   PHP-FPM image

Notes   -   Changed the whole file structure

### 1.1 
Added   -   Gulp
Added   -   .env
Added   -   Composer Image
Added   -   Config files for Gulp
Added   -   Config files for Composer

### 1.2
Added   -   Custom scripts that will read and setup wp-config.php

Notes   -   The scripts read and sets up wp-config.php accordingly to how you configured .env file

### 1.3
Added   -   Support for wordpress multisite

### 1.4
Fixed   -   Changed php-fpm image since the old one caused issues

### 1.4.2
Added   -   Custom scripts that will read and fix your sql dump

Notes   -   Sometimes people forget to dump the database without CREATE DATABASE in the sql, the script will fix it for you.

### 1.4.4
Fixed   -   xDebug not working.
Fixed   -   MYSQL Image downloadling latest casued issues
Fixed   -   Issue where scripts couldnt locate database
Fixed   -   Scripts would create SQL file releated to wordpress even though wordpress wasnt installed

### 1.4.5
Added   -   conf files for MYSQL etc/mysql/my.cnf

### 1.4.6
Added   -   ssl conf file (wp-ssl)

---

## Help us !

Any thought, feedback or (hopefully not!)

Developed by [laha@html24.net](Lars Hagen)
