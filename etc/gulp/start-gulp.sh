#!/bin/sh

if [ -z "$THEME_NAME" ]; then
	echo "--- You need to set the variable THEME_NAME in .env for gulp to work. ---"
	return 0
fi
until cd /var/www/html/$THEME_LOCATION$THEME_NAME
do
    sleep 2
    echo "retrying"
done
if [ ! -f /var/www/md5.txt ]; then
	echo "--- First Install ---"
    md5sum package.json > /var/www/md5.txt
	npm set progress=false
	npm rm --global gulp --silent 	#I spent 5 months trying to figure this out.
									#Go to Lars if you want to why know we are uninstalling gulp before we even installed it
	echo "--- Installing GULP, please wait... ---"
	npm i -g gulp --silent
	echo "--- Running npm install, please wait... ---"
    npm install --save-dev --silent
    echo "--- First Install Finished ---"
else
	x=`md5sum package.json`
	y=`cat /var/www/md5.txt`
	if [ "$x" == "$y" ]; then
		echo "--- It looks NPM is up-to-date, npm install will run every time there are changes in package.json ... ---"
	else
		echo "--- New changes in package.json -> running npm install, please wait... ---"
		echo "--- Update Install ---"
		md5sum package.json > /var/www/md5.txt
		npm set progress=false
		npm rm --global gulp --silent 	#I spent 5 months trying to figure this out.
										#Go to Lars if you want to why know we are uninstalling gulp before we even installed it		npm i -g gulp
		echo "--- Installing GULP, please wait... ---"
		npm i -g gulp --silent
		echo "--- Running npm install, please wait... ---"
	    npm install --save-dev --silent
		echo "--- Update Install Finished ---"
	fi
fi
eval $GULP_COMMAND