#!/usr/bin/env bash

if [[ ! -z "${XDEBUG}" ]]; then
  if [ "${XDEBUG}" == "TRUE" ]; then     
  	mv /etc/php/7.3/conf.d/xdebug.ini.disable /etc/php/7.3/conf.d/xdebug.ini
  fi
  if [ "${XDEBUG}" == "FALSE" ]; then     
  	mv /etc/php/7.3/conf.d/xdebug.ini /etc/php/7.3/conf.d/xdebug.ini.disable
  fi
fi

tarfile=`find ./ -maxdepth 1 -name "*.tar" -print`
if [ ! -z "$tarfile" ] && [ "$tarfile" != "./extracted.tar" ]; then
	echo "--- Extracting $tarfile, please wait...---"
	(pv -f -i 3 $tarfile | tar xf - ) 2>&1 | tr '\r' '\n'
	mv $tarfile extracted.tar
	echo "--- Renamed temp.tar to extracted.tar, so we dont extract it everytime run the project ---"
fi
if [ ! -z "$DUMP_URL" ]; then
	if [ ! -f /var/www/html/extracted.tar ]; then
		wget --progress=dot:giga $DUMP_URL -O "temp.tar"
		echo "--- Extracting temp.tar, please wait...---"
		(pv -f -i 3 temp.tar | tar xf - ) 2>&1 | tr '\r' '\n'
		mv temp.tar extracted.tar
		echo "--- Extracted temp.tar ---"
		echo "--- Renamed temp.tar to extracted.tar, so we dont extract it everytime run the project ---"
	fi
fi

robotInstall=0
if [ ! -e "./wp-config.php" ] &&  [ ! -e "./wp-config-sample.php" ]
then
	echo "============================================"
	echo "A robot is now installing WordPress for you."
	echo "============================================"
	#download wordpress
	curl -O https://wordpress.org/latest.tar.gz
	#unzip wordpress
	tar -zxvf latest.tar.gz
	#change dir to wordpress
	cd wordpress
	#copy file to parent dir
	cp -rf . ..
	#move back to parent dir
	cd ..
	#remove files from wordpress folder
	rm -R wordpress
	
	echo "Cleaning..."
	#remove zip file
	rm latest.tar.gz
	echo "========================="
	echo "Installation is complete."
	echo "========================="
	robotInstall=1

fi

if [ -e "./wp-config.php" ] && [ $robotInstall == 0 ]
then

	#if ! grep "WP_HOME" wp-config.php
	#then
		#sed -i "/wp-settings/i\define('WP_HOME', '$WP_URL');\ndefine('WP_SITEURL', '$WP_URL');" wp-config.php
		#echo "define('WP_HOME', '$WP_URL');" >> wp-config.php
		#echo "define('WP_SITEURL', '$WP_URL');" >> wp-config.php
	#fi
	sed -i "/DB_USER/c\define('DB_USER', '$MYSQL_ROOT_USER');" wp-config.php
	sed -i "/DB_PASSWORD/c\define('DB_PASSWORD', '$MYSQL_ROOT_PASSWORD');" wp-config.php

	sed -i "/DB_HOST/c\define('DB_HOST', '$MYSQL_HOST');" wp-config.php
	sed -i "/DB_NAME/c\define('DB_NAME', '$MYSQL_DATABASE');" wp-config.php
	counter=0
	until [ "$(curl -v --silent mysql:3306 2>&1 | grep native)" ];
	do
		counter=$((counter+1))
		if [ $counter = 10 ]; then
			echo --- MySQL is starting, please wait... ---
			counter=0
		fi
		sleep 1
	done

	if [ ! $(mysql -u "$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" -hmysql "$MYSQL_DATABASE" -sse "SELECT COUNT(DISTINCT table_name) FROM information_schema.columns WHERE table_schema = '$MYSQL_DATABASE'") -gt 0 ];
	then
		echo "--- MYSQL is running ---"
	    echo "--- Finding the Database Dump ---"
	    cd ../db
		sqlfile=`find ./ -name "*.sql" -print `
		#check if sql dump has create database 
		if [ ! -z "$sqlfile" ] && grep -q "CREATE DATABASE" "$sqlfile"
		then
			echo "--- Important Notice(START) : We found \"CREATE DATABASE\" in "$sqlfile", it shouldn't be there.  ---"
			echo Created a backup of "$sqlfile" and placed it in /put-database-dump-here
			echo "In $sqlfile we removed Line "$(grep -n -m 1 "CREATE DATABASE" "$sqlfile")
			echo "In $sqlfile we removed Line "$(grep -n -m 1 "USE" "$sqlfile")
			echo "--- Important Noice(END) : Please review the two removed lines above and make sure it looks correct, in case they dont look correct we made a backup --- "
			sed -i.bak -e '1,/CREATE DATABASE/{/CREATE DATABASE/d}' -e '1,/USE/{/USE/d}' "$sqlfile"
			#remove create database
		fi
		if [ -z "$sqlfile" ]; then
			cd ../html
			sqlfile=`find ./ -maxdepth 1 -name "*.sql" -print`
			#check if sql dump has create database 
			if grep -q "CREATE DATABASE" "$sqlfile" && [ ! -z "$sqlfile" ]
			then
				echo "--- Important Notice(START) : We found \"CREATE DATABASE\" in "$sqlfile", it shouldn't be there.  ---"
				echo Created a backup of "$sqlfile" and placed it in /web
				echo "In $sqlfile we removed Line "$(grep -n -m 1 "CREATE DATABASE" "$sqlfile")
				echo "In $sqlfile we removed Line "$(grep -n -m 1 "USE" "$sqlfile")
				echo "--- Important Noice(END) : Please review the two removed lines above and make sure it looks correct, in case they dont look correct we made a backup --- "
				sed -i.bak -e '1,/CREATE DATABASE/{/CREATE DATABASE/d}' -e '1,/USE/{/USE/d}' "$sqlfile"
				#remove create database
			fi
		fi
		echo "--- Importing Database, please wait... ---"
		#import sql file
		echo "$sqlfile"
		
		wp --path=/var/www/html/ --require=/var/www/_supress_errors.php db import "$sqlfile"
		cd ../html
		#get url from .env and format it
		WP_URL_FORMATTED=`echo $WP_URL | awk -F[/:] '{print $4}'`
		echo --- MySQL is running, the database will now be updated with new links, please wait... ---
		
		#get siteurl and remove everything but domain name "https://example.com/" becomes "example.com"
		SITEURL=`wp --path=/var/www/html/ --require=/var/www/_supress_errors.php option get siteurl | awk -F[/:] '{print $4}'`
		echo "--- Replacing $SITEURL with $WP_URL_FORMATTED ---"

		wp --path=/var/www/html/ --require=/var/www/_supress_errors.php --allow-root search-replace --all-tables "$SITEURL" "$WP_URL_FORMATTED" --skip-columns=guid
		
		#get siteurl again, this will return an empty string if DOMAIN_CURRENT_SITE in wp-config dosen't match the new urls in the database
		SITEURL=`wp --path=/var/www/html/ --require=/var/www/_supress_errors.php --allow-root option get siteurl | awk -F[/:] '{print $4}'`
		if [ -z "${SITEURL}" ]; then
			echo "--- ^ If you are getting site not found errors above, you shouldnt worry. ^ ---"
		    sed -i "/DOMAIN_CURRENT_SITE/c\define('DOMAIN_CURRENT_SITE', '$WP_URL_FORMATTED');" wp-config.php
		    sed -i "/SUBDOMAIN_INSTALL/c\define('SUBDOMAIN_INSTALL', 'false');" wp-config.php
		fi
		echo "--- Database imported ---"   
	fi
	#the stuff below is WIP 
	#sed -i "/WP_SITEURL/c\define('WP_SITEURL', '$WP_URL');" wp-config.php
	#sed -i "/WP_HOME/c\define('WP_HOME', '$WP_URL');" wp-config.php
fi

if [ -e "$DRUPAL_SETTINGS_LOCATION" ]
then
	chmod 755 "$DRUPAL_SETTINGS_LOCATION"
	cd "$DRUPAL_SETTINGS_LOCATION"
	sed -i "/'username'/c\'username' => '$MYSQL_ROOT_USER'," settings.php
	sed -i "/'password'/c\'password' => '$MYSQL_ROOT_PASSWORD'," settings.php
	sed -i "/'database'/c\'database' => '$MYSQL_DATABASE'," settings.php
	sed -i "/'host'/c\'host' => '$MYSQL_HOST'," settings.php
	counter=0
	until [ "$(curl -v --silent mysql:3306 2>&1 | grep native)" ];
	do
		counter=$((counter+1))
		if [ $counter = 10 ]; then
			echo --- MySQL is starting, please wait... ---
			counter=0
		fi
		sleep 1
	done
	if [ ! $(mysql -u "$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" -hmysql "$MYSQL_DATABASE" -sse "SELECT COUNT(DISTINCT table_name) FROM information_schema.columns WHERE table_schema = '$MYSQL_DATABASE'") -gt 0 ];
	then
		cd /var/www/db/
		sqlfile=`find ./ -name "*.sql" -print`
		if grep -q "CREATE DATABASE" "$sqlfile" && [ ! -z "$sqlfile" ]
		then
			echo "--- Important Notice(START) : We found \"CREATE DATABASE\" in "$sqlfile", it shouldn't be there.  ---"
			echo Created a backup of "$sqlfile" and placed it in /put-database-dump-here
			echo "In $sqlfile we removed Line "$(grep -n -m 1 "CREATE DATABASE" "$sqlfile")
			echo "In $sqlfile we removed Line "$(grep -n -m 1 "USE" "$sqlfile")
			echo "--- Important Noice(END) : Please review the two removed lines above and make sure it looks correct, in case they dont look correct we made a backup --- "
			sed -i.bak -e '1,/CREATE DATABASE/{/CREATE DATABASE/d}' -e '1,/USE/{/USE/d}' "$sqlfile"
			#remove create database
		fi
		echo "--- Importing Database, please wait... ---" 
		mysql -u "$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE" < "$sqlfile"
		echo "--- Database imported ---" 
	fi
fi

echo --- php-fpm is running ---
/usr/sbin/php-fpm -F